#!/usr/bin/env bash

CONTAINER_PREFIX="firma_dev"
CONTAINER_APACHE="${CONTAINER_PREFIX}_apache"
CONTAINER_MYSQL="${CONTAINER_PREFIX}_mysql"
CONTAINER_GULP="${CONTAINER_PREFIX}_gulp"
CONTAINER_POSTFIX="${CONTAINER_PREFIX}_postfix"
MYSQL_DATA_DIR="./volumes/mysql-data"
bold=$(tput bold)
normal=$(tput sgr0)
CONTAINER_UID=$(id -u)
export CONTAINER_UID

usage () {
	echo -e "Uzycie:
	$0 <polecenie>
	
	Polecenia:
	composer_install - zainstalowanie bibliotek niezbednych do dzialania 
		stron poprzez wykonanie composer install w katalogu 
		../www/www_firma_pl
	start - uruchomienie srodowiska. Pierwsze uruchomienie powoduje 
		pobranie obrazow i zbudowanie kontenerow, co moze potrwac
		kilka minut.
	stop - zatrzymanie srodowiska 
	restart - zatrzymanie i ponowne uruchomienie srodowiska
	status - wyswietl status srodowiska 
	destroy - usuniecie srodowiska 
	logs - wyswietla logi
	logs -f - wyswietla logi i pozostaje w trybie nasluchiwania"
	exit 1
}

composer_install () {
	docker exec -t "${CONTAINER_APACHE}" /usr/bin/sudo -u www-data /usr/local/bin/composer install -d /var/www/www_firma_pl/
}


check_prerequisites () {
	check_docker_installed 
	check_docker_compose_installed 
#	check_git_installed
}

check_docker_installed () {
	docker -v
	if [[ $? -ne 0 ]] ; then
		echo -e "\n# ${bold}Nie znaleziono zainstalowanego dockera.${normal}"
		echo -e "# Zainstaluj dockera zgodnie z instrukcja: https://docs.docker.com/install/"
		exit 1
	fi
	echo -e "# docker jest zainstalowany\n"
}

check_docker_compose_installed () {
	docker-compose version
	if [[ $? -ne 0 ]] ; then
		echo -e "\n# ${bold}Nie znaleziono zainstalowanego docker-compose.${normal}"
		echo -e "# Zainstaluj docker-compose zgodnie z instrukcja: https://docs.docker.com/compose/install/#install-compose" 
		exit 1
	fi
	echo -e "# docker-compose jest zainstalowany\n"
}

check_git_installed () {
	git --version
	if [[ $? -ne 0 ]] ; then
		echo -e "\n# ${bold}Nie znaleziono zainstalowanego gita.${normal}"
		echo -e "# Zainstaluj gita zgodnie z instrukcja: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git" 
		exit 1
	fi
	echo -e "# git jest zainstalowany\n"
}

start () {
	check_prerequisites
	
	echo -e "\n# Startuje lokalne srodowisko deweloperskie"
	docker-compose up -d
	echo -e "\n# Status kontenerow"
	status
	echo -e "\n# Uruchomione uslugi:
----------------------------------------
  Strona: https://localhost:3000
----------------------------------------
  Browsersync UI: http://localhost:3001
----------------------------------------
  MySQL: localhost:43306
  MySQL user: user
  MySQL pass: pass 
----------------------------------------"
}

stop () {
	echo -e "\n# Zatrzymuje lokalne srodowisko deweloperskie"
	docker-compose stop
}

restart () {
	stop
	start
}

status () {
	docker ps -a -f name=$CONTAINER_PREFIX --format "table {{.Names}}\t{{.Status}}\t{{.RunningFor}}\t{{.Ports}}"
}

destroy() {
	echo -e "\n${bold}Zamierzasz zatrzymac srodowisko deweloperskie i usunac wszystkie zwiazane z nim kontenery ${normal}"
	read -r -p "Czy jestes pewien? [t/N]: " response
	response=${response,,} # tolower

	if [[ "$response" =~ ^(tak|t)$ ]] ; then
		docker-compose down --rmi local
	fi

	if [[ "$(ls -A $MYSQL_DATA_DIR)" ]] ; then
		echo -e "\n${bold}W katalogu $MYSQL_DATA_DIR znajduja sie pliki z danymi serwera MySQL ${normal}"
		read -r -p "Czy chcesz je usunac? [t/N]: " response_mysql
		response=${response_mysql,,} # tolower

		if [[ "$response_mysql" =~ ^(tak|t)$ ]] ; then
			rm -rv ./volumes/mysql-data/*
		fi
	fi
}	 

logs () {
	docker-compose logs $@
}

if [ $# -eq 0 ] ; then
	usage
fi

HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(start|stop|restart|status|destroy|logs|composer_install)$ ]]; then
	"$HANDLER" "$@"
else
	usage  
fi

