Dev Docker Environment
=========

Skrypt dev.sh budujący kontenery i konfigurujący lokalne środowisko deweloperskie dla www.firma.pl oparte o:

- *Docker*
- *Apache2*
- *MySQL*
- *Gulp (z browserSync)*

Wymagania:
------------

- Zainstalowany *docker*
- Zainstalowany *docker-compose*
- Użytkownik dodany do grupy docker
- katalog ../www zawiera katalogi ze stronami www:
	$ ls ../www/
	admin_firma_pl  html  core  merch_firma_pl  www_firma_pl
- katalog ../www/www_firma_pl zawiera link symboliczny do core: www_firma_pl/core -> ../core
- katalog ./volumes/docker-entrypoint-initdb.d/ zawiera dump bazy w formacie sql.gz (aktualny plik można ściągnąć z /home/mysql-dumps/firma.dump.sql.gz na sandboxie - jest on tworzony codziennie o 7:00)

Istotne pliki 
------------
- **./apache/files/env** - plik .env do użycia w www.firma.pl
- **./gulp/gulpfile.js** - plik konfiguracyjny do gulpa

Uruchomienie
------------
Start środowiska: 

    ./dev.sh start

Instalacja bibliotek używanych przez stronę: 

    ./dev.sh composer_install
