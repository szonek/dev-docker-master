//Deklaracja zmiennych
var gulp = require('gulp');
var browserSync = require('browser-sync').create(); //automatyczne przeładowanie przeglądarki
var sass = require('gulp-sass'); //kompilacja SASS-a do .css
var rename = require("gulp-rename"); //zmiana ścieżki plików .css
var path = require("path");
var imagemin = require('gulp-imagemin'); //optymalizacja grafiki
var cache = require('gulp-cache'); //optymalizacja grafiki
var watch = require('gulp-watch'); //nasłuchiwanie zmian
var useref = require('gulp-useref'); //konkatenacja i minifikacja css
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var gutil = require('gulp-util'); //konkatynacja i minifikacja js
var concat = require('gulp-concat');
var babili = require('gulp-babili');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var del = require('del'); //czyszczenie katalogu dist
var runSequence = require('run-sequence'); //automatyczne wykonywanie tasków, task default


function ourErrorHandler(error) {
    console.log(gutil.colors.red(error.toString()));
    this.emit('end');
}

//Definicja tasków
gulp.task('browserSync', function () {
    browserSync.init({
	files: "www/**",
        proxy: 'https://apache:4443',
	open: false
    })
});

gulp.task('sass', function () {

    return gulp.src('./public/scss/orderCreate.scss')
        .pipe(plumber({
            errorHandler : ourErrorHandler
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: "compressed"
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 version']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public/css/page'))
        .pipe(browserSync.stream({match: "**/*.css"}));
});


gulp.task('images', function () {
    return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'))
});

gulp.task('watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
    gulp.watch("**/*.html").on("change", browserSync.reload);
});

gulp.task('useref', function () {
    return gulp.src('app/*.html')
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});

gulp.task('scripts', function () {
    return gulp.src(['app/js/*.js'])
        .pipe(concat('main.min.js'))
        .pipe(babili({
            mangle: {
                keepClassNames: true
            }
        }))
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gulp.dest('dist/js'));
});

gulp.task('clean:dist', function() {
    return del.sync('dist');
});

gulp.task('default', function (callback) {
    console.log(' ----- Rozpoczynamy pracę ----- ');
    runSequence(['watch', 'sass', 'browserSync'],
        callback
    )
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', ['default', 'images'], 'useref', 'scripts',
        callback)
});

